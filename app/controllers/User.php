<?php
class user extends controller {
  public function index() {
    $data["judul"] = "user";
    $this->view("templates/header", $data);
    $this->view("user/index");
    $this->view("templates/footer");

  }
  public function profile($nama = "linux", $pekerjaan = "devs") {
    $data["judul"] = "user";
    $data["nama"] = $nama;
    $data["pekerjaan"] = $pekerjaan;
    $this->view("templates/header", $data);
    $this->view("user/profile", $data);
    $this->view("templates/footer");
  }

  
}