<?php
class blog extends controller {
  public function index () {
    $data["judul"] = "blog";
    $data["blog"] =$this->model("blog_model")->getAllBlog();
     $this->view("templates/header", $data);
     $this->view("blog/index", $data);
     $this->view("templates/footer");
  }
  public function detail($id) {
    $data['judul'] = "detail blog";
    $data['blog'] = $this->model("Blog_model"->getBlogById);
    $this->view('templates/header', $data);
    $this->view('blog/detail', $data);
    $this->view('templates/footer');
  }
}