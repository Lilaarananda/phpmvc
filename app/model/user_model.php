<?php
class user_model {
  private $table = 'user';
  private $db;

  public function __construct() {
    $this->db = new database;
  }

  public function getUserById($id) {
    $this->db->query("SELECT * FROM {$this->table} WHERE id=:id");
    $this->db->bind('id', $id);
    return $this->db->resultSingle();
  }
  
  public function getAllUser() {
    $this->db->query("SELECT * FROM $this->table");
    return $this->db->resultAll();
  }
  public function getUserByEmail($email){
    $this->db->query("SELECT * FROM user WHERE email = :email");
    $this->db->bind('email', $email);
    return $this->db->resultSingle();
}
public function tambahUser($data) 
{ 

  
    $password = md5($data['password'] . SALT); 

    $query = ("INSERT INTO $this->table VALUES (NULL, :username, :email, :password)");
    $this->db->query($query);
    $this->db->bind('username', $data['username']);
    $this->db->bind('email', $data['email']);
    $this->db->bind('password', $password);

    $this->db->execute();
    
    return $this->db->rowCount();
}
public function ubahUser($data){
    $query =("UPDATE $this->table SET penulis = :penulis, email = :email WHERE id_penulis");
    $this->db->query($query);
    $this->db->bind('penulis', $data['penulis']);
    $this->db->bind('email', $data['email']);
    $this->db->bind('id_penulis', $data['id_penulis']);
    $this->db->execute();
}
public function hapusUser($id){
    $query = ("DELETE FROM $this->table WHERE id_penulis = :id_penulis");
    $this->db->query($query);
    $this->db->bind('id_penulis', $id);
    $this->db->execute();
}
}
?>