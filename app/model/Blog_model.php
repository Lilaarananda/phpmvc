<?php
class Blog_model {
  private $table = 'blog';
  private $db;

  public function __construct() {
    $this->db = new database;
  }

  public function getUserById($id) {
    $this->db->query("SELECT * FROM {$this->table} WHERE id=:id");
    $this->db->bind('id', $id);
    return $this->db->resultSingle();
  }
  
  public function getAllBlog() {
    $this->db->query("SELECT * FROM $this->table");
    return $this->db->resultAll();
  }
}
?>